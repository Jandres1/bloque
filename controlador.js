let bloque = document.getElementById("bloque");
let rangoY = document.getElementById("posicionY");
let movY = Number(bloque.offsetTop);

rangoY.oninput = function(){
    bloque.style.top = (movY + Number(rangoY.value)) + "px";
}

let rangoX = document.getElementById("posicionX");
let movX = Number(bloque.offsetLeft);

rangoX.oninput = function()
{
    bloque.style.left = (movX + Number(rangoX.value)) + "px";
}

let configT = document.getElementsByName("size")[0];

configT.oninput = function(){
    bloque.style.transform = "scale("+ configT.value +","+ configT.value +")";
}

let opaco = document.getElementsByName("opacidad")[0];

opaco.oninput = function(){
    bloque.style.opacity = opaco.value;
}

let forma = document.getElementsByName("forma")[0];
let accept = document.getElementsByName("boton")[0];

accept.onclick = function(){
    if(forma.value == "circulo")
        bloque.style.borderRadius = "50%";

    if(forma.value == "cuadro"){
        bloque.style.borderRadius = "0";
        bloque.style.transform = "none";
    }

    if(forma.value == "rombo")
        bloque.style.transform = "rotate(45deg)";
}

let hexa = document.getElementsByName("hexadecimal")[0];
let color;
hexa.onkeypress = function(e) 
{
    if(e.which == 13)
    {
        color = hexa.value;
        if(color != "" && color.length < 7)
            bloque.style.backgroundColor = "#"+hexa.value;            
    }
}


let rojo = document.getElementsByName("r")[0];
let verde = document.getElementsByName("g")[0];
let azul = document.getElementsByName("b")[0];
let alpha = document.getElementsByName("a")[0];

rojo.oninput = function()
{
    bloque.style.backgroundColor = "rgba("+rojo.value+","+verde.value+","+azul.value+","+alpha.value+")";
}
verde.oninput = function()
{
    bloque.style.backgroundColor = "rgba("+rojo.value+","+verde.value+","+azul.value+","+alpha.value+")";
}
azul.oninput = function()
{
    bloque.style.backgroundColor = "rgba("+rojo.value+","+verde.value+","+azul.value+","+alpha.value+")";
}
alpha.oninput = function()
{
    bloque.style.backgroundColor = "rgba("+rojo.value+","+verde.value+","+azul.value+","+alpha.value+")";
}

